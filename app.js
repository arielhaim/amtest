var express = require('express');
var mime = require('mime-types');

var app = express();
express.static.mime.define({'video/mpeg': ['mp4']});

var port = 1202;

app.use(express.static('public'));
app.use(express.static('src/views'));

app.get('/', function (req, res){
    res.send('Hello World!');
});

app.listen(port, function(err){
    console.log('Hello World');
});